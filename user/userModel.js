var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var userSchema = new Schema({
	'userID' : String,
	'password' : String,
	'email' : String,
	'perfilusuario' : Array,
	'fechaCreate' : String,
	'fechaUpdate' : String,
	'logueado'	:	Boolean	
});

module.exports = mongoose.model('user', userSchema);
