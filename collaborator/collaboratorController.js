var collaboratorModel = require("./collaboratorModel.js");
var staffModel = require("../staff/staffModel.js");

/**
 * collaboratorController.js
 *
 * @description :: Server-side logic for managing collaborators.
 */
module.exports = {
  /**
   * collaboratorController.list()
   */
  list: function (req, res) {
    let indModificado = req.query.modificado;

    let jsonQuery = {};

    console.log(req.query);

    let indNoAsignado =
      req.query.noasignado !== undefined
        ? new Boolean(req.query.noasignado).valueOf()
        : false;

    if (indModificado !== undefined)
      jsonQuery.modificado = new Boolean(indModificado).valueOf();
    if (req.query.disciplina !== undefined)
      jsonQuery.disciplina = req.query.disciplina;

    if (indNoAsignado) {
      staffModel.distinct(
        "userID",
        { disciplina: jsonQuery.disciplina },
        function (err, users) {
          console.log(users);
          if (err) {
            return res.status(500).json({
              message: "Error when getting staff.",
              error: err
            });
          }
          if (!users) {
            return res.status(404).json({
              message: "No such proyectos"
            });
          }

          collaboratorModel.find({ userID: { $nin: users }, disciplina: jsonQuery.disciplina }, function (
            err,
            colaboradoresNoAsignados
          ) {
            if (err) {
              return res.status(500).json({
                message: "Error when getting collaborator.",
                error: err
              });
            }
            if (!colaboradoresNoAsignados) {
              return res.status(404).json({
                message: "No such collaborator"
              });
            }
            console.log(colaboradoresNoAsignados);
            return res.json(colaboradoresNoAsignados);
          });
        }
      );
    } else {
      collaboratorModel.find(jsonQuery, function (err, collaboModificados) {
        if (err) {
          return res.status(500).json({
            message: "Error when getting collaborator.",
            error: err
          });
        }
        if (!collaboModificados) {
          return res.status(404).json({
            message: "No such collaborator"
          });
        }

        return res.json(collaboModificados);
      });
    }
  },

  /**
   * collaboratorController.show()
   */
  show: function (req, res) {
    var id = req.params.id;
    collaboratorModel.findOne({ userID: id }, function (err, collaborator) {
      if (err) {
        return res.status(500).json({
          message: "Error when getting collaborator.",
          error: err
        });
      }
      if (!collaborator) {
        return res.status(404).json({
          message: "No such collaborator"
        });
      }
      return res.json(collaborator);
    });
  },

  /**
   * collaboratorController.create()
   */
  create: function (req, res) {
    var collaborator = new collaboratorModel({
      empleadoID: req.body.empleadoID,
      perfil: req.body.perfil,
      entorno: req.body.entorno,
      distribuido: req.body.distribuido,
      funcional: req.body.funcional,
      tecnico: req.body.tecnico,
      ether: req.body.ether
    });

    collaborator.save(function (err, collaborator) {
      if (err) {
        return res.status(500).json({
          message: "Error when creating collaborator",
          error: err
        });
      }
      return res.status(201).json(collaborator);
    });
  },

  /**
   * collaboratorController.update()
   */
  update: function (req, res) {
    var id = req.params.id;
    collaboratorModel.findOne({ userID: id }, function (err, collaborator) {
      if (err) {
        return res.status(500).json({
          message: "Error when getting collaborator",
          error: err
        });
      }
      if (!collaborator) {
        return res.status(404).json({
          message: "No such collaborator"
        });
      }

      collaborator.userID = req.body.userID? req.body.userID: collaborator.userID;
      collaborator.perfil = req.body.perfil? req.body.perfil: collaborator.perfil;
      collaborator.entorno = req.body.entorno? req.body.entorno: collaborator.entorno;
      collaborator.distribuido = req.body.distribuido? req.body.distribuido: collaborator.distribuido;
      collaborator.funcional = req.body.funcional? req.body.funcional: collaborator.funcional;
      collaborator.tecnico = req.body.tecnico? req.body.tecnico: collaborator.tecnico;
      collaborator.ether = req.body.ether ? req.body.ether : collaborator.ether;
      collaborator.modificado = req.body.modificado ? req.body.modificado : false;
      collaborator.skillsAlto = req.body.skillsAlto ? req.body.skillsAlto : collaborator.skillsAlto;
      collaborator.skillsMedio = req.body.skillsMedio ? req.body.skillsMedio : collaborator.skillsMedio;
      collaborator.skillsBajo = req.body.skillsBajo ? req.body.skillsBajo : collaborator.skillsBajo;

      collaborator.save(function (err, collaborator) {
        if (err) {
          return res.status(500).json({
            message: "Error when updating collaborator.",
            error: err
          });
        }

        return res.json(collaborator);
      });
    });
  },

  /**
   * collaboratorController.remove()
   */
  remove: function (req, res) {
    var id = req.params.id;
    collaboratorModel.findByIdAndRemove(id, function (err, collaborator) {
      if (err) {
        return res.status(500).json({
          message: "Error when deleting the collaborator.",
          error: err
        });
      }
      return res.status(204).json();
    });
  }
};
