var express = require('express');
var router = express.Router();
var collaboratorController = require('./collaboratorController.js');

/*
 * GET
 */
router.get('/', collaboratorController.list);

/*
 * GET
 */
router.get('/:id', collaboratorController.show);

/*
 * POST
 */
router.post('/', collaboratorController.create);

/*
 * PUT
 */
router.put('/:id', collaboratorController.update);

/*
 * DELETE
 */
router.delete('/:id', collaboratorController.remove);

module.exports = router;
