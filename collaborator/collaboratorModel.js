var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var collaboratorSchema = new Schema({
	'userID' : String,
	'nombre' : String,
	'apepaterno' : String,
	'apematerno' : String,
	'lider' : String,
	'anexo' : String,
	'disciplina' : String,
	'especialidad' : String,
	'email' : String,
	
	'perfil' : Array,
	'entorno' : Array,
	'distribuido' : Array,
	'funcional' : Array,
	'tecnico' : Array,
	'ether' : Array,
	'building_block' : String,
	'modificado' : Boolean,
	'skillsBajo' : Array,
	'skillsMedio' : Array,
	'skillsAlto' : Array
});

module.exports = mongoose.model('collaborator', collaboratorSchema);
