var ninjaModel = require('./ninjaModel.js');

/**
 * ninjaController.js
 *
 * @description :: Server-side logic for managing ninjas.
 */
module.exports = {

    /**
     * ninjaController.list()
     */
    list: function (req, res) {
        ninjaModel.find(function (err, ninjas) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting ninja.',
                    error: err
                });
            }
            return res.json(ninjas);
        });
    },

    /**
     * ninjaController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        ninjaModel.findOne({userID: id}, function (err, ninja) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting ninja.',
                    error: err
                });
            }
            if (!ninja) {
                return res.status(404).json({
                    message: 'No such ninja'
                });
            }
            return res.json(ninja);
        });
    },

    /**
     * ninjaController.create()
     */
    create: function (req, res) {
        var ninja = new ninjaModel({
			userID : req.body.userID,
			puntos : req.body.puntos,
			cinturon : req.body.cinturon,
			fecha_cinturon : req.body.fecha_cinturon

        });

        ninja.save(function (err, ninja) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating ninja',
                    error: err
                });
            }
            return res.status(201).json(ninja);
        });
    },

    /**
     * ninjaController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        ninjaModel.findOne({userID: id}, function (err, ninja) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting ninja',
                    error: err
                });
            }
            if (!ninja) {
                return res.status(404).json({
                    message: 'No such ninja'
                });
            }

            ninja.userID = req.body.userID ? req.body.userID : ninja.userID;
			ninja.puntos = req.body.puntos ? req.body.puntos : ninja.puntos;
			ninja.cinturon = req.body.cinturon ? req.body.cinturon : ninja.cinturon;
			ninja.fecha_cinturon = req.body.fecha_cinturon ? req.body.fecha_cinturon : ninja.fecha_cinturon;
			
            ninja.save(function (err, ninja) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating ninja.',
                        error: err
                    });
                }

                return res.json(ninja);
            });
        });
    },

    /**
     * ninjaController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        ninjaModel.findByIdAndRemove(id, function (err, ninja) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the ninja.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
