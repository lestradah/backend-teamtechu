var express = require('express');
var router = express.Router();
var ninjaController = require('./ninjaController.js');

/*
 * GET
 */
router.get('/', ninjaController.list);

/*
 * GET
 */
router.get('/:id', ninjaController.show);

/*
 * POST
 */
router.post('/', ninjaController.create);

/*
 * PUT
 */
router.put('/:id', ninjaController.update);

/*
 * DELETE
 */
router.delete('/:id', ninjaController.remove);

module.exports = router;
