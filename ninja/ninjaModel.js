var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var ninjaSchema = new Schema({
	'userID' : String,
	'puntos' : Number,
	'cinturon' : String,
	'fecha_cinturon' : String
});

module.exports = mongoose.model('ninja', ninjaSchema);
