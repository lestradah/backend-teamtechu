var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var staffSchema = new Schema({
	'userID' : String,
	'fechaCreate' : String,
	'fechaUpdate' : String,
	'disciplina' : String,
	'especialidad' : String,
	'proyecto' : String
});

module.exports = mongoose.model('staff', staffSchema);
