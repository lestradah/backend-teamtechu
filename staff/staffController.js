var staffModel = require("./staffModel.js");
var collaboratorModel = require("../collaborator/collaboratorModel.js");

/**
 * staffController.js
 *
 * @description :: Server-side logic for managing staffs.
 */
module.exports = {
  /**
   * staffController.list()
   */
  list: function(req, res) {
    console.log(req.query);
    staffModel.find(function(err, staffs) {
      if (err) {
        return res.status(500).json({
          message: "Error when getting staff.",
          error: err
        });
      }
      return res.json(staffs);
    });
  },

  /**
   * staffController.show()
   */
  show: function(req, res) {
    var id = req.params.id;
    staffModel.find({ userID: id }, function(err, staff) {
      if (err) {
        return res.status(500).json({
          message: "Error when getting staff.",
          error: err
        });
      }
      if (!staff) {
        return res.status(404).json({
          message: "No such staffing"
        });
      }
      return res.json(staff);
    });
  },

  /**
   * staffController.create()
   */
  create: function(req, res) {
    var staff = new staffModel({
      empleadoID: req.body.empleadoID,
      fechaCreate: req.body.fechaCreate,
      fechaUpdate: req.body.fechaUpdate,
      datastaff: req.body.datastaff
    });

    staff.save(function(err, staff) {
      if (err) {
        return res.status(500).json({
          message: "Error when creating staff",
          error: err
        });
      }
      return res.status(201).json(staff);
    });
  },

  /**
   * staffController.update()
   */
  update: function(req, res) {
    var id = req.params.id;
    staffModel.findOne({ userID: id }, function(err, staff) {
      if (err) {
        return res.status(500).json({
          message: "Error when getting staff",
          error: err
        });
      }
      if (!staff) {
        return res.status(404).json({
          message: "No such staffing"
        });
      }

      staff.empleadoID = req.body.empleadoID
        ? req.body.empleadoID
        : staff.empleadoID;
      staff.fechaCreate = req.body.fechaCreate
        ? req.body.fechaCreate
        : staff.fechaCreate;
      staff.fechaUpdate = req.body.fechaUpdate
        ? req.body.fechaUpdate
        : staff.fechaUpdate;
      staff.datastaff = req.body.datastaff
        ? req.body.datastaff
        : staff.datastaff;

      staff.save(function(err, staff) {
        if (err) {
          return res.status(500).json({
            message: "Error when updating staff.",
            error: err
          });
        }

        return res.json(staff);
      });
    });
  },

  /**
   * staffController.remove()
   */
  remove: function(req, res) {
    var id = req.params.id;
    staffModel.findByIdAndRemove(id, function(err, staff) {
      if (err) {
        return res.status(500).json({
          message: "Error when deleting the staff.",
          error: err
        });
      }
      return res.status(204).json();
    });
  },

  /**
   * staffController.proyecto()
   */
  proyecto: function(req, res) {
    var id = req.params.id;
    console.log(id);
    staffModel.findOne(
      { "datastaff.sdatool": id },
      { "datastaff.$": 1, _id: 0 },
      function(err, staff) {
        if (err) {
          return res.status(500).json({
            message: "Error when getting staff.",
            error: err
          });
        }
        if (!staff) {
          return res.status(404).json({
            message: "No such staffing"
          });
        }
        return res.json(staff);
      }
    );
  },

  reporte: function(req, res) {
    // 1 - reporte staffing
    // 2 - reporte colaborator
    let tipoReporte = req.params.id;

    if (tipoReporte == 1) {
     reporteStaffing(req, res);
    } else if (tipoReporte == 2) {
     reporteColaborador(req, res);
    }
  },

  consulta: function(req, res) {
    let jsonQuery = {};

    jsonQuery.disciplina = decodeURI(req.query.disciplina);
    if (req.query.especialidad !== undefined)
      jsonQuery.especialidad = req.query.especialidad;

    console.log(req.query);

    if (new Boolean(req.query.onlyproyecto).valueOf()) {
      staffModel.distinct("proyecto", jsonQuery, function(err, proyecto) {
        console.log(proyecto);
        if (err) {
          return res.status(500).json({
            message: "Error when getting staff.",
            error: err
          });
        }
        if (!proyecto) {
          return res.status(404).json({
            message: "No such proyectos"
          });
        }
        return res.json(proyecto);
      });
    } else {
      if (req.query.proyecto !== undefined)
        jsonQuery.proyecto = req.query.proyecto;

      console.log(req.query);
      console.log(JSON.stringify(jsonQuery));

      staffModel.find(jsonQuery).sort({userID : 1}).exec(function(err, staff) {
        //staffModel.find({'disciplina': disciplina}, function (err, staff) {
        if (err) {
          return res.status(500).json({
            message: "Error when getting staff.",
            error: err
          });
        }
        if (!staff) {
          return res.status(404).json({
            message: "No such staffing"
          });
        }
        return res.json(staff);
      });
    }
  }
};

function reporteStaffing(req, res) {
 
  
  staffModel.aggregate(
    [
      {
        $group: {
          _id: { disciplina: "$disciplina" },
          asignados: { $addToSet: "$userID" }
        }
      },
      { $sort: { _id: 1 } },
      { $project: { asignados: { $size: "$asignados" } } }
    ],
    function(err, staff) {
      if (err) {
        return res.status(500).json({
          message: "Error when getting staff",
          error: err
        });
      }
      if (!staff) {
        return res.status(404).json({
          message: "No such staffing"
        });
      }

      console.table(staff);
      collaboratorModel.aggregate(
        [
          {
            $group: {
              _id: { disciplina: "$disciplina" },
              asignados: { $addToSet: "$userID" }
            }
          },
          { $sort: { _id: 1 } },
          { $project: { asignados: { $size: "$asignados" } } }
        ],
        function(err, collaborator) {
          if (err) {
            return res.status(500).json({
              message: "Error when getting staff.",
              error: err
            });
          }
          if (!collaborator) {
            return res.status(404).json({
              message: "No such staffing"
            });
          }

          let resp = [];
          console.table(collaborator);

          collaborator.map((collaborador, index) => {
            let reporte = {};
            reporte.disciplina = collaborador._id.disciplina;
            reporte.cantidadAsignados = staff[index].asignados;
            reporte.cantidadNoAsignados =
              collaborador.asignados - staff[index].asignados;

            resp.push(reporte);
          });

          setTimeout(function(){
            return res.json(resp);
          }, 3000);
        }
      );
    }
  );
}

function reporteColaborador(req, res) {

    collaboratorModel.find({modificado: true}, function (err, collaboModificaddos) {
        if (err) {
            return res.status(500).json({
                message: 'Error when getting collaborator.',
                error: err
            });
        }
        if (!collaboModificaddos) {
            return res.status(404).json({
                message: 'No such collaborator'
            });
        }
        return res.json(collaboModificaddos);
    });


}
