var express = require('express');
var router = express.Router();
var staffController = require('./staffController.js');

/*
 * GET
 */
router.get('/', staffController.consulta);

/*
 * GET
 */
router.get('/:id', staffController.show);

/*
 * POST
 */
router.post('/', staffController.create);

/*
 * PUT
 */
router.put('/:id', staffController.update);

/*
 * DELETE
 */
router.delete('/:id', staffController.remove);

/*
 * PROYECTO
 */
router.get('/proyecto/:id', staffController.proyecto);

/*
 * DISCIPLINA
 */
router.get('/reporte/:id', staffController.reporte);

/*
 * QUERY
 */
//router.get('/consulta/:disciplina', staffController.consulta);


module.exports = router;
