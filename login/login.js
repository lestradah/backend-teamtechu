const express = require('express')
const userController = require('../user/userController')
//const auth = require('../middleware/auth')

const router = express.Router()

// curl -s  -H 'Content-type: application/json' -X POST http://localhost:4000/users -d '{"userID" : "enzo dff", "password":"pepito1"}' | jq

router.post('/login', userController.login);
router.post('/logout', userController.logout);

module.exports = router
