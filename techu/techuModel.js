var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var techuSchema = new Schema({
	'userID' : String,
	'cursos' : Array
});

module.exports = mongoose.model('techu', techuSchema);
