var express = require('express');
var router = express.Router();
var techuController = require('./techuController.js');

/*
 * GET
 */
router.get('/', techuController.list);

/*
 * GET
 */
router.get('/:id', techuController.show);

/*
 * POST
 */
router.post('/', techuController.create);

/*
 * PUT
 */
router.put('/:id', techuController.update);

/*
 * DELETE
 */
router.delete('/:id', techuController.remove);

module.exports = router;
