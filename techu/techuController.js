var techuModel = require('./techuModel.js');

/**
 * techuController.js
 *
 * @description :: Server-side logic for managing techus.
 */
module.exports = {

    /**
     * techuController.list()
     */
    list: function (req, res) {
        techuModel.find(function (err, techus) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting techu.',
                    error: err
                });
            }
            return res.json(techus);
        });
    },

    /**
     * techuController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        techuModel.findOne({userID: id}, function (err, techu) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting techu.',
                    error: err
                });
            }
            if (!techu) {
                return res.status(404).json({
                    message: 'No such techu'
                });
            }
            return res.json(techu);
        });
    },

    /**
     * techuController.create()
     */
    create: function (req, res) {
        var techu = new techuModel({
			userID : req.body.userID,
			cursos : req.body.cursos

        });

        techu.save(function (err, techu) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating techu',
                    error: err
                });
            }
            return res.status(201).json(techu);
        });
    },

    /**
     * techuController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        techuModel.findOne({userID: id}, function (err, techu) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting techu',
                    error: err
                });
            }
            if (!techu) {
                return res.status(404).json({
                    message: 'No such techu'
                });
            }

            techu.userID = req.body.userID ? req.body.userID : techu.userID;
			techu.cursos = req.body.cursos ? req.body.cursos : techu.cursos;
			
            techu.save(function (err, techu) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating techu.',
                        error: err
                    });
                }

                return res.json(techu);
            });
        });
    },

    /**
     * techuController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        techuModel.findByIdAndRemove(id, function (err, techu) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the techu.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
